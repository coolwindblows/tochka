const gulp          = require('gulp'),
      rename        = require('gulp-rename'),
      pug           = require('gulp-pug'),
      sass          = require('gulp-sass'),
      uglify        = require('gulp-uglify'),
      plumber       = require('gulp-plumber'),
      webpackStream = require('webpack-stream'),
      autoprefixer  = require('gulp-autoprefixer'),
      browsersync   = require('browser-sync'),
      reload        = browsersync.reload,
      paths         = {
                        pug:['./src/pug/index.pug'],
                        sass:['./src/sass/main.sass'],
                        js:['./src/scripts/common.js']
                      };

gulp.task('server', function() {
  browsersync.init({
    open: false,
    notify: false,
    server: {
      baseDir: "./dist",
    }
  });
});

gulp.task('scripts', function () {
  return gulp.src(paths.js)
    .pipe(plumber())
    .pipe(webpackStream({
      output: {
        filename: 'common.js',
      },
      module: {
        rules: [
          {
            test: /\.(js)$/,
            exclude: /(node_modules)/,
            loader: 'babel-loader',
            query: {
              presets: ['env']
            }
          }
        ]
      },
      externals: {}
    }))
    .pipe(gulp.dest('./dist/scripts/'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./dist/scripts/'))
    .pipe(browsersync.reload({stream : true}));
});

gulp.task('sass', function () {
  return gulp.src(paths.sass)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer(['last 15 versions']))
    .pipe(gulp.dest('./dist/css/'))
    .pipe(browsersync.reload({stream : true}));
});

gulp.task('pug', function () {
  return gulp.src(paths.pug)
    .pipe(plumber())
    .pipe(pug({pretty : true}))
    .pipe(gulp.dest('./dist'))
    .pipe(browsersync.reload({stream : true}));
});

gulp.task('watch', () => {
  gulp.watch('src/**/*.pug', ['pug']);
  gulp.watch('src/**/**/*.sass', ['sass']);
  gulp.watch('src/**/*.js', ['scripts']);
});

gulp.task('default', ['sass', 'pug', 'server', 'watch', 'scripts']);