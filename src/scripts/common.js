'use strict';

console.log('works');

const buttonWrapper = document.querySelector('.about-wrapper'),
      buttonLink = document.getElementById('linkAbout');

function contrastButton(){
  buttonWrapper.style.cssText = 'filter: contrast(200%);';
};

function contrastButtonOff(){
  buttonWrapper.style.cssText = 'filter: contrast(100%);';
};

buttonLink.addEventListener('mouseover', function(){
  contrastButton();
});

buttonLink.addEventListener('mouseleave', function(){
  contrastButtonOff();
});

console.log(buttonLink);